import datetime
import unittest
import requests

def fetch_coviddata_statewise():
    """ Fetch covid data """
    current_date = datetime.date.today()
    # api-endpoint
    url_data = "https://api.covid19india.org/v2/state_district_wise.json"

    # defining a params dict for the parameters to be sent to the API
    param_passed = {'date': current_date}

    # sending get request and saving the response as response object
    rquest_data = requests.get(url=url_data, params=param_passed)

    # extracting data in json format
    data = rquest_data.json()
    return data[1:]

DATA_FETCH = fetch_coviddata_statewise()

class TestStringMethods(unittest.TestCase):
    """ Unit Tests for fetch covid data """
    # Returns True if len of data is 36 which includes 29 states and 7 union territories
    def tests_len_data(self):
        self.assertEqual(len(DATA_FETCH), 36)
        # Returns True if the string is in upper case.

    #Check no state name is empty
    def test_state_name(self):
        self.assertEqual(DATA_FETCH[0].get("state") is not None, True)

if __name__ == '__main__':

    unittest.main()
    print(fetch_coviddata_statewise())
