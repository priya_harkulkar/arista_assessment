import sys
import os
import logging
from multiprocessing import Process, Manager
import paramiko
import psycopg2
from paramiko.auth_handler import AuthenticationException


# def get_private_key():
#     k = paramiko.RSAKey.from_private_key_file("/home/priya/.ssh/id_rsa")
#     return k

def get_cpu_cores(client):
    """
    Execute multiple commands in succession.

    :param client: List of unix commands as strings.
    :returns command output list
    """
    try:
        stdin, stdout, stderr = client.exec_command("lscpu | awk NR==7'{print $4}'")
        return stdout.readlines()
    except Exception as ex:
        logger.exception("Unable to fetch device cores information an excp was encountered : ", ex)
    return []


def connect(hostname, private_key, return_dict):
    """
    Open connection to remote host.
    :param
        hostname : name of the host
        private_key : private key to use for authentication

    """
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.connect(hostname=hostname, username='admin', pkey=private_key)
        cpu_cores = get_cpu_cores(ssh_client)
        return_dict[hostname] = cpu_cores
    except AuthenticationException as error:
        logger.error('Authentication failed: \
                        did you remember to create an SSH key? %s', error)
        # raise error
    except Exception as error:
        logger.error("Exception ", error)


def close_connection(client):
    """Close ssh connection."""
    client.close()

# def pg_connection():
#     """
#     """
#     conn = psycopg2.connect("dbname=arista_data user=postgres password=postgres")
#     cur = conn.cursor()
#     content = key.encode('utf-8')
#     # print(content)
#     encoded = base64.b64encode(content)
#     query = "INSERT INTO ssh_keys (id, name, public_key) VALUES (1, 'priya@2.tcp.ngrok.io', %s)" % (psycopg2.Binary(encoded))
#     print(query)
#     cur.execute(query)
#     conn.commit()
#     conn.close()
#     return conn


def pg_fetch_key(hostname):
    """
    Fetch SSH key stored in postgres db
    :param hostname: Name of the host
    :returns key
    """
    conn = psycopg2.connect("dbname=arista_data user=postgres password=postgres")
    cursor = conn.cursor()
    sql = "SELECT p_key FROM ssh_keys where name='%s'" % hostname
    cursor.execute(sql)
    public_key = cursor.fetchall()
    # for i in cursor.fetchall():
    #     print(i)
    #     pickle.loads(b"".join(cursor.fetchall()))
    conn.close()
    return public_key


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    filename = sys.argv[1]  # passing file name as a parameter

    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(THIS_FOLDER, filename)

    manager = Manager()
    return_dict = manager.dict()  # shared variable

    # Check whether the specified path is an existing file
    if os.path.isfile(path):
        fl = open(filename, "r")
        host_list = fl.read().split(",")

        for host in range(len(host_list)):

            processes = []
            key = pg_fetch_key(host)
            # Create the process, and connect it to the worker function
            new_process = Process(target=connect, args=(host, key, return_dict))

            # Add new process to the list of processes
            processes.append(new_process)

            # Start the process
            new_process.start()
        for process in processes:
            process.join()
        for i in return_dict:
            print("Hostname:{}, CPU cores{}".format(i, return_dict[i]))
    else:
        logger.error("Not a valid file or directory")
